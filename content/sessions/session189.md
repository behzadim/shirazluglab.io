---
title: "جلسه ۱۸۹"
author: "مریم بهزادی"
date: "1399-01-27"
description: "معرفی نرم‌افزار آزاد KiCad"
subtitle: "معرفی نرم‌افزار آزاد KiCad"
keywords: "شیرازلاگ، نرم افزار آزاد، الکترونیک، kicad، طراحی برد، طراحی pcb" 
weight: -1
summaryImage: "/img/posters/poster189.jpg"
tags: []
images: []
readmore: false
draft: false
---
[![نشست ۱۸۹ شیرازلاگ معرفی نرم افزار KiCad توسط امین کشاورزی](/img/posters/poster189.jpg)](/img/posters/poster189.jpg)